package com.hakam.contactsapp.contract;

import com.hakam.contactsapp.model.PostsModelItem

interface PostsContract {

    interface Presenter{
        fun getAllPosts()
    }

    interface View {
        fun initActivity();
        fun initListener();
        fun onLoadingPosts(loading: Boolean)
        fun onResultPosts(posts: List<PostsModelItem>)
    }
}
