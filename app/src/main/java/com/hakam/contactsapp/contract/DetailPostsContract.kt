package com.hakam.contactsapp.contract

import com.hakam.contactsapp.model.DetailPostModel

interface DetailPostsContract {

    interface Presenter{
        fun getDetailPost()
    }

    interface View {
        fun initActivity()
        fun initListener()
        fun onLoading(loading: Boolean)
        fun onResultDetailPost(title: String, body: String)
    }
}