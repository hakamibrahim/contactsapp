package com.hakam.contactsapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.hakam.contactsapp.R
import com.hakam.contactsapp.model.Constant
import com.hakam.contactsapp.model.DetailPostModel
import com.hakam.contactsapp.model.PostsModel
import com.hakam.contactsapp.model.PostsModelItem

class PostsAdapter(var context: Context, var posts: ArrayList<PostsModelItem>, var listener: OnAdapterListener) :
    RecyclerView.Adapter<PostsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.adapter_posts, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val posts = posts[position]

        holder.title.text = posts.title
        holder.body.text = posts.body

        holder.card.setOnClickListener {
            Constant.POST_ID = posts.id
            listener.onClick(posts)
            //Toast.makeText(context, posts.id.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val card: CardView = itemView.findViewById(R.id.card_posts)
        val title: TextView = itemView.findViewById(R.id.txt_title_posts)
        val body: TextView = itemView.findViewById(R.id.txt_body_posts)
    }

    fun setData(newData: List<PostsModelItem>) {
        posts.clear()
        posts.addAll(newData)
        notifyDataSetChanged()
    }

    interface OnAdapterListener{
        fun onClick(posts: PostsModelItem)
    }
}