package com.hakam.contactsapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.hakam.contactsapp.R
import com.hakam.contactsapp.contract.DetailPostsContract
import com.hakam.contactsapp.model.DetailPostModel
import com.hakam.contactsapp.presenter.DetailPostsPresenter
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity(), DetailPostsContract.View {

    lateinit var presenter: DetailPostsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        presenter = DetailPostsPresenter(this)
    }

    override fun onStart() {
        super.onStart()
        presenter.getDetailPost()
    }

    override fun initActivity() {
    }

    override fun initListener() {
    }

    override fun onLoading(loading: Boolean) {

    }

    override fun onResultDetailPost(title: String, body: String) {
        val title = intent.getStringExtra("DETAIL_TITLE")
        txt_title_detail.text = title

        val body = intent.getStringExtra("DETAIL_TITLE")
        txt_body_detail.text = body
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}