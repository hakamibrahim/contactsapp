package com.hakam.contactsapp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.hakam.contactsapp.R
import com.hakam.contactsapp.adapter.PostsAdapter
import com.hakam.contactsapp.contract.PostsContract
import com.hakam.contactsapp.model.DetailPostModel
import com.hakam.contactsapp.model.PostsModelItem
import com.hakam.contactsapp.presenter.PostsPresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), PostsContract.View {

    private var DETAIL_TITLE: String = "DETAIL_TITLE"
    private var DETAIL_BODY: String = "DETAIL_BODY"

    lateinit var posAdapter: PostsAdapter
    lateinit var presenter: PostsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = PostsPresenter(this)
    }

    override fun onStart() {
        super.onStart()
        presenter.getAllPosts()
    }

    override fun initActivity() {

    }

    override fun initListener() {
        posAdapter = PostsAdapter(this, arrayListOf(), object : PostsAdapter.OnAdapterListener{
            override fun onClick(posts: PostsModelItem) {
                val intent = Intent(this@MainActivity, DetailActivity::class.java)
                intent.putExtra(DETAIL_TITLE, posts)
            }
        })
        rc_all_posts.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = posAdapter
        }
    }

    override fun onLoadingPosts(loading: Boolean) {

    }

    override fun onResultPosts(posts: List<PostsModelItem>) {
        posAdapter.setData(posts)
    }
}