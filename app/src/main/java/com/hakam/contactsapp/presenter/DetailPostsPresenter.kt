package com.hakam.contactsapp.presenter

import android.util.Log
import com.hakam.contactsapp.api.ApiService
import com.hakam.contactsapp.contract.DetailPostsContract
import com.hakam.contactsapp.model.Constant
import com.hakam.contactsapp.model.DetailPostModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailPostsPresenter(val view: DetailPostsContract.View) : DetailPostsContract.Presenter {
    private val TAG = "DetailPostsPresenter"

    init {
        view.initActivity()
        view.initListener()
    }

    override fun getDetailPost() {
        ApiService.endpoint.getDetailPost()
            .enqueue(object : Callback<DetailPostModel> {
                override fun onResponse(
                    call: Call<DetailPostModel>,
                    response: Response<DetailPostModel>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        view.onResultDetailPost(data!!.title, data.body)
                        //Log.d(TAG, data.body)
                    }
                }

                override fun onFailure(call: Call<DetailPostModel>, t: Throwable) {
                    t.message
                }
            })
    }
}