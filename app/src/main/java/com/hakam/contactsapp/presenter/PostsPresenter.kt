package com.hakam.contactsapp.presenter

import android.util.Log
import android.view.View
import com.hakam.contactsapp.api.ApiService
import com.hakam.contactsapp.contract.PostsContract
import com.hakam.contactsapp.model.PostsModel
import com.hakam.contactsapp.model.PostsModelItem
import com.hakam.contactsapp.view.MainActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PostsPresenter(private val view: PostsContract.View) : PostsContract.Presenter {

    private val TAG = "PostsPresenter"

    init {
        view.initActivity()
        view.initListener()
    }

    override fun getAllPosts() {
        ApiService.endpoint.getAllPost().enqueue(object : Callback<List<PostsModelItem>> {
            override fun onResponse(
                call: Call<List<PostsModelItem>>,
                response: Response<List<PostsModelItem>>
            ) {
                if (response.isSuccessful) {
                    val data = response.body()
                    try {
                        if (data != null) {
                            view.onResultPosts(data)
                            Log.d(TAG, "data tersedia\n$data")
                        } else {
                            Log.d(TAG, "data tidak tersedia")
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<List<PostsModelItem>>, t: Throwable) {
                t.message
            }
        })
    }
}