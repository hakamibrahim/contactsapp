package com.hakam.contactsapp.api

import com.hakam.contactsapp.model.DetailPostModel
import com.hakam.contactsapp.model.PostsModelItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiEndpoint {

    @GET("posts")
    fun getAllPost(): Call<List<PostsModelItem>>

    @GET("posts/{id}")
    fun getDetailPost(
    ): Call<DetailPostModel>
}